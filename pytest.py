# 剑指 Offer 32 - III. 从上到下打印二叉树 III

class Solution(object):
    def levelOrder(self, root):
        if not root: return []
        list1, queue=[], collections.deque()
        queue.append(root)
        while queue:
            layer=[]
            for i in range(len(queue)):
                node=queue.popleft()
                layer.append(node.val)
                if node.left:queue.append(node.left)
                if node.right:queue.append(node.right)
            list1.append(layer)
            if not queue: break
            layer=[]
            for i in range(len(queue)):
                node=queue.pop()
                layer.append(node.val)
                
                if node.right:queue.appendleft(node.right)
                if node.left:queue.appendleft(node.left)
            list1.append(layer)
        return list1